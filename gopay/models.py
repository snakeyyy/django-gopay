# -*- coding: utf-8 -*-
__author__ = 'snakey'

from exceptions import NotImplementedError
import SOAPpy
from SOAPpy import SOAPProxy

from django.db import models

from gopay import convert_float_to_gopay_price
from gopay import settings as gopay_settings,  create_payment_command
from gopay.api.CountryCode import CountryCode
from gopay.api.GopayHelper import PaymentStatus

from gopay.api.SSLHelper import VerifiedHTTPSTransport
from gopay.api.GopayHelper import GopayHelper

import unicodedata


class OrderError(Exception):
    pass

class OrderManager(models.Manager):
    def paid(self):
        return self.filter(state=PaymentStatus.STATE_PAID)


STATE_CHOICES = (
    ('CREATED_LOCAL', u'Vytvořeno lokálně'),
    (PaymentStatus.STATE_CREATED, u'Vytvořeno v GoPay'),
    (PaymentStatus.STATE_PAYMENT_METHOD_CHOSEN, u'Platební metoda zvolena'),
    (PaymentStatus.STATE_AUTHORIZED, u'Autorizována'),
    (PaymentStatus.STATE_PAID, u'Zaplacena'),
    (PaymentStatus.STATE_CANCELED, u'Zrušena'),
    (PaymentStatus.STATE_TIMEOUTED, u'Timeouted'),
    (PaymentStatus.STATE_REFUNDED, u'Refunded'),
    (PaymentStatus.STATE_PARTIALLY_REFUNDED, u'Partially refunded'),
)


class Order(models.Model):
    order_number = models.CharField(u'Číslo objednávky', max_length=128, blank=True, null=True)  #pokud neni vyplneno, pouzije se ID
    product_title = models.CharField(u'Jméno produktu', max_length=128)
    price = models.FloatField(u'Cena')
    currency = models.CharField(u'Měna', max_length=10, default='CZK')
    payment_session_id = models.BigIntegerField(u'GoPay session id',  blank=True, null=True, db_index=True)

    pre_authorization = models.BooleanField(u'Preautorizovana platba', default=False)
    recurrent_payment = models.BooleanField(u'Rekurentni platba', default=False)
    recurrence_date_to = models.DateField(u'Rekurentní platba do', blank=True, null=True)
    recurrence_cycle = models.CharField(u'Cyklus rekurentni platby', max_length=10,  blank=True, null=True)
    recurrence_period = models.PositiveSmallIntegerField(u'Perioda rekurentni platby', default=1)

    first_name = models.CharField(u'Jméno', max_length=128, blank=True, null=True)
    last_name = models.CharField(u'Příjmení', max_length=128, blank=True, null=True)
    city = models.CharField(u'Město', max_length=128,  blank=True, null=True)
    street = models.CharField(u'Ulice', max_length=256,  blank=True, null=True)
    postal_code = models.CharField(u'PSČ', max_length=20, blank=True, null=True)
    country_code = models.CharField(u'Kód země', max_length=5,  blank=True, null=True)
    email = models.EmailField(u'email')
    phone = models.CharField(u'Tel. číslo', max_length=20,  blank=True, null=True)

    p1 = models.CharField(u'Extra parametr 1', max_length=128, blank=True, null=True)
    p2 = models.CharField(u'Extra parametr 2', max_length=128,  blank=True, null=True)
    p3 = models.CharField(u'Extra parametr 3', max_length=128,  blank=True, null=True)
    p4 = models.CharField(u'Extra parametr 4', max_length=128,  blank=True, null=True)

    state = models.CharField(u'Stav objednávky', default='CREATED_LOCAL', choices=STATE_CHOICES, max_length=60)
    created_on = models.DateTimeField(u'Založeno', auto_now_add=True)
    updated_on = models.DateTimeField(u'Naposledy upraveno',auto_now=True)


    objects = OrderManager()

    def get_order_id(self):
        if self.order_number:
            return self.order_number
        else:
            if not self.id:
                raise OrderError('You have to save order first.')
            return self.id

    def create_payment_command(self, server, allowed_channels=gopay_settings.GOPAY_ALLOWED_PAYMENT_CHANNELS, default_channel=gopay_settings.GOPAY_DEFAULT_PAYMENT_CHANNEL):
        data = self.get_data_for_gopay()
        return create_payment_command(server, data, allowed_channels, default_channel)

    def get_data_for_gopay(self):
        if self.recurrent_payment:
            raise NotImplementedError('Reccurent payments not implemented yet.')

        data = {}
        if self.order_number:
            data['orderNumber'] = self.order_number
        else:
            data['orderNumber'] = str(self.id)
        product_name = unicodedata.normalize('NFKD', self.product_title).encode('ascii', 'ignore')
        data['productName'] = product_name

        data['totalPrice'] = convert_float_to_gopay_price(self.price)
        data['currency'] = str(self.currency)
        data['firstName'] = self.first_name
        data['lastName'] = self.last_name
        data['email'] = self.email
        data['phoneNumber'] = self.phone
        data['postalCode'] = self.postal_code
        data['street'] = self.street
        data['city'] = self.city

        if self.country_code:
            data['countryCode'] = getattr(CountryCode, self.country_code)
        data['p1'] = self.p1
        data['p2'] = self.p2
        data['p3'] = self.p3
        data['p4'] = self.p4
        return data

    def create_payment_in_gopay(self, allowed_channels=gopay_settings.GOPAY_ALLOWED_PAYMENT_CHANNELS, default_channel=gopay_settings.GOPAY_DEFAULT_PAYMENT_CHANNEL):
        server = SOAPProxy(gopay_settings.GOPAY_URL, transport=VerifiedHTTPSTransport)
        payment_command = self.create_payment_command(server, allowed_channels, default_channel)
        payment_return = server.createPayment(payment_command)
        payment_session_id = payment_return.paymentSessionId
        soappy_payment_session_id = SOAPpy.longType(payment_session_id)
        self.payment_session_id = soappy_payment_session_id._data
        self.state = PaymentStatus.STATE_CREATED
        self.save()

    def get_redirect_url_to_gate(self):
        if not self.payment_session_id:
            raise OrderError('You have to call create_payment_in_gopay_first')
        server = SOAPProxy(gopay_settings.GOPAY_URL, transport=VerifiedHTTPSTransport)

        identity = server.Types.EPaymentSessionInfo
        identity.targetGoId = SOAPpy.longType(gopay_settings.GOPAY_ESHOP_GOID)
        identity.paymentSessionId = SOAPpy.longType(self.payment_session_id)
        GopayHelper().sign(identity, gopay_settings.GOPAY_SECURE_KEY)
        encrypted_signature = identity.encryptedSignature
        url_to_redirect = gopay_settings.GOPAY_REDIRECT + "?sessionInfo.targetGoId=%s&sessionInfo.paymentSessionId=%s\
                                                        &sessionInfo.encryptedSignature=%s" \
                          % (gopay_settings.GOPAY_ESHOP_GOID, self.payment_session_id, encrypted_signature)
        return url_to_redirect

    def __unicode__(self):
        return "%s" % self.id

    class Meta:
        ordering = ('updated_on',)
        verbose_name = u'Objednávka'
        verbose_name_plural = u'GoPay objednávky'













